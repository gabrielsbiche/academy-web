import type { AppProps } from "next/app";
import { ApolloProvider } from "@apollo/client";

import "../styles/globals.css";
import { client } from "../src/apollo/index";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <ApolloProvider client={client}>
      <Component {...pageProps} />
    </ApolloProvider>
  )
}
