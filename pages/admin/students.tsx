import type { NextPage } from "next";
import Link from "next/link";

import {
  Student,
  useDeleteOneStudentMutation,
  useGetStudentsQuery,
} from "../../graphql/generated";

const Students: NextPage = () => {
  const { data } = useGetStudentsQuery();
  const [deleteOneStudent] = useDeleteOneStudentMutation();

  const deleteOne = async (id: string) => {
    deleteOneStudent({
      variables: {
        input: {
          id: id,
        },
      },
      refetchQueries: ["GetStudents"],
    });
  };

  return (
    <div>
      <h1> Students </h1>
      
      <Link href={"/"}>Home</Link>

      {data?.students?.nodes?.map((student: Student) => (
        <h1 key={student.id} onClick={() => deleteOne(student?.id)}>
          {student.name}
        </h1>
      ))}
    </div>
  );
};

export default Students;
