# Academy WEB

## Tecnologias utilizadas
NextJs, GraphQL, Apollo-client e Codegen.

## API
A API deste projeto pode ser encontrada [aqui](https://gitlab.com/gabrielsbiche/academy-api).

## Siga os passos abaixo para executar o projeto

### 1° Passo - Configure as variáveis de ambiente para conexão com a API
Renomeie o arquivo da raiz do projeto chamado .env.example para .env e defina a URL da API

### 2° Passo - Instale as dependências 
```
$ yarn install 
```

### 3° Passo - Execute a aplicação
```
$ yarn dev 
```


