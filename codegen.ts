import { CodegenConfig } from '@graphql-codegen/cli'

const config: CodegenConfig = {
  schema: process.env.NEXT_PUBLIC_API_URL, 
  documents: ['./src/graphql/*.graphql'], 
    generates: {
    'graphql/generated.ts': {
      plugins: ['typescript', 'typescript-operations', 'typescript-react-apollo'],
      config: {
        withHooks: true
      },
    }
  }
}

export default config
